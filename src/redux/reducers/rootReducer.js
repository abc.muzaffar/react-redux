import { combineReducers } from "redux";

import counterReducer from "./counterReducer";
import movieReducer from "./movieReducer";

const rootReducer = combineReducers({
  counter: counterReducer,
  movie: movieReducer,
});

export default rootReducer;
