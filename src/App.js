import React from "react";
import Counter from "./components/Counter";
import GlobalState from "./components/GlobalState/GlobalState";
import ComponentA from "./components/ComponentA/ComponentA";

const App = () => {
  return (
    <div className="App">
      <h1>App Component</h1>
      <pre>
        State: <code>None</code>
      </pre>
      <pre>
        Child: <code>Counter, ComponentA</code>
      </pre>
      <div className="wrapper">
        <Counter />

        {/* // ! Only for Visualisation */}
        <GlobalState />
        {/* // ! Only for Visualisation */}

        <ComponentA />
      </div>
    </div>
  );
};

export default App;
